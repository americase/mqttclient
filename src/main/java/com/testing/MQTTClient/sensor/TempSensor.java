package com.testing.MQTTClient.sensor;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;


/**
 *
 * @author abdis
 */
public class TempSensor extends Sensor{

    public TempSensor() throws MqttException {
        super("home/sensor/temp");
    }
    
    @Override
    public boolean send() {
        if ( !client.isConnected()) {
            return false;
        }
        byte[] payload = getTemp().getBytes();
        MqttMessage message = new MqttMessage(payload);
        message.setQos(2);
        try {
            client.publish(topic, message);
        } catch (MqttException ex) {
            return false;
        }
        return true;
    }
    
    private String getTemp(){
        double maxTemp = 60.0;
        double minTemp = -60.0;
        double cur_temp = Math.random() * (maxTemp - minTemp + 1 ) + minTemp;
        return getJSON(cur_temp);
    }

    private String getJSON(double cur_temp) {
        String JSON = "{ \"sensorID\" : \""+uuid+"\", \"topic\" : \""+topic+"\", \"content\"  : \""+cur_temp+"\" }";
        return JSON;
    }
    
}
