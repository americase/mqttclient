package com.testing.MQTTClient.sensor;

import com.testing.MQTTClient.Main;
import java.util.UUID;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 *
 * @author abdis
 */
public abstract class Sensor {
    
    protected final String uuid;
    protected final IMqttClient client;
    protected final String topic;

    protected Sensor(String topic) throws MqttException {
        this.uuid = UUID.randomUUID().toString();
        this.client = new MqttClient(Main.BROKER_ADDRESS, uuid);
        this.client.connect();
        this.topic = topic;
    }

    public String getUUID() {
        return uuid;
    }

    public IMqttClient getClient() {
        return client;
    }

    public String getTopic() {
        return topic;
    }
    
    public abstract boolean send();
    
    public void closeConnection() throws MqttException{
        client.disconnect();
        client.close();
    }
    
}
