/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.testing.MQTTClient.sensor;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 *
 * @author abdis
 */
public class LightSensor extends Sensor{

    public LightSensor() throws MqttException {
        super("home/sensor/light");
    }

    @Override
    public boolean send() {
        if ( !client.isConnected()) {
            return false;
        }
        byte[] payload = getTemp().getBytes();
        MqttMessage message = new MqttMessage(payload);
        message.setQos(2);
        try {
            client.publish(topic, message);
        } catch (MqttException ex) {
            return false;
        }
        return true;
    }
    
    private String getTemp(){
        double cur_intensity = Math.random() * 100;
        return getJSON(cur_intensity);
    }

    private String getJSON(double cur_intensity) {
        String JSON = "{ \"sensorID\" : \""+uuid+"\", \"topic\" : \""+topic+"\", \"content\"  : \""+cur_intensity+"\" }";
        return JSON;
    }
    
}
