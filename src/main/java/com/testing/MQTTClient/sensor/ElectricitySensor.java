/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.testing.MQTTClient.sensor;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 *
 * @author abdis
 */
public class ElectricitySensor extends Sensor{

    public ElectricitySensor() throws MqttException {
        super("home/sensor/electricity");
    }

    @Override
    public boolean send() {
        if ( !client.isConnected()) {
            return false;
        }
        byte[] payload = getTemp().getBytes();
        MqttMessage message = new MqttMessage(payload);
        message.setQos(2);
        try {
            client.publish(topic, message);
        } catch (MqttException ex) {
            return false;
        }
        return true;
    }
    
    private String getTemp(){
        double minpower = 600.0;
        double maxpower = 6000.0;
        double cur_power = Math.random() * (maxpower - minpower + 1 ) + minpower;
        return getJSON(cur_power);
    }

    private String getJSON(double cur_kWh) {
        String JSON = "{ \"sensorID\" : \""+uuid+"\", \"topic\" : \""+topic+"\", \"content\"  : \""+cur_kWh+"\" }";
        return JSON;
    }
    
}
