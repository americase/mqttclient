package com.testing.MQTTClient;

import com.testing.MQTTClient.sensor.ElectricitySensor;
import com.testing.MQTTClient.sensor.LightSensor;
import com.testing.MQTTClient.sensor.Sensor;
import com.testing.MQTTClient.sensor.TempSensor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 *
 * @author abdis
 */
public class Main extends Application {

    public static String BROKER_ADDRESS = "tcp://192.168.1.55:1883";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);

    }

    public static boolean continueSending = false;

    @Override
    public void start(Stage stage) throws Exception {
        Button send = new Button("Send Messages");
        VBox mainLayout = new VBox(send);
        mainLayout.setAlignment(Pos.CENTER);
        Scene root = new Scene(mainLayout);
        stage.setScene(root);
        stage.setTitle("Send new temp via MQTT");
        stage.setResizable(false);
        try {
            final Sensor[] sensors = new Sensor[]{new LightSensor(), new TempSensor(), new ElectricitySensor()};
            send.setOnMouseClicked((t) -> {
                try {
                    double index = Math.random()*100;
                    if(index<10){
                        sensors[0].send();
                    }else if(index>=10 && index <60){
                        sensors[1].send();
                    }else{
                        sensors[2].send();
                    }
                } catch (Exception e) {
                    //
                }
            });
            stage.setOnCloseRequest((t) -> {
                try {
                    for (Sensor sensor : sensors) {
                        sensor.closeConnection();
                    }
                } catch (MqttException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        } catch (MqttException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        stage.setHeight(100);
        stage.setWidth(300);
        stage.show();
    }

}
